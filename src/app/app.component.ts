import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name: string = 'Catalão';

  constructor() {
    this.changeName('Filipe');
  }

  changeName(name:string): void {
    this.name = name;
  }
}
